import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { NoAuthNavigator } from "./src/stacks/NoAuthNavigator";
import { AuthNavigation } from "./src/stacks/AuthNavigation";
import * as firebase from "firebase";

export default function App() {
  const [status, setStatus] = useState(false);

  useEffect(() => {
    // Initialize Firebase
    var firebaseConfig = {
      apiKey: "AIzaSyDFMCW8wX3Y4njsHFsZruJph9z4XF9PTlk",
      authDomain: "monkeycoin-440f5.firebaseapp.com",
      projectId: "monkeycoin-440f5",
      storageBucket: "monkeycoin-440f5.appspot.com",
      messagingSenderId: "688059331494",
      appId: "1:688059331494:web:7fd4222116dfd98dda1567",
      measurementId: "G-DTB7C8CY4X",
    };
    // Initialize Firebase
    try {
      if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig);
        console.log("done");
      }
    } catch (error) {
      alert("Check you internet connection!!");
    }

    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        setStatus("LoggedIn");
        console.log("User logged in");
      } else {
        console.log("User logged OUT");
        setStatus(false);
      }
    });
  }, []);
  return (
    <View style={styles.container}>
      <NoAuthNavigator />
      {/* <AuthNavigation /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

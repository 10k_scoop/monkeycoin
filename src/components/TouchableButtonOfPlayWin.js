import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import colorsStyles from '../styles/colors.styles';

const TouchableButtonOfPlayWin = props => {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.button}>
      <Text style={styles.text}>{props.text}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  button: {
    // height: 45,
    // width: '35%',
    padding: 40,
    paddingVertical: 10,
    // alignSelf: 'center',
    backgroundColor: colorsStyles.CLR_ORANGE,
    borderRadius: 150 / 2,
    // justifyContent: 'center',
  },

  text: {
    color: colorsStyles.CLR_WHITE,
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});
export default TouchableButtonOfPlayWin;

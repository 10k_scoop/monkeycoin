import React from 'react';
import {View, Text} from 'react-native';
import colorsStyles from '../styles/colors.styles';
import dimensions from '../styles/dimensions';
import InsetShadow from "react-native-inset-shadow";
const PlayWinView1 = props => {
  return (
    <InsetShadow
        containerStyle={{
        shadowRadius: 3,
        borderRadius: 13,
        marginTop: 20,
        shadowOpacity: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 20,
        shadowOffset: {
          width: 2,
          height: 2,
        },
        backgroundColor: colorsStyles.CLR_PRIMARY,
        width: dimensions.windowWidth * 0.31,
        height: dimensions.windowHeight * 0.07,
      }}>
      <View
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          left: 10,
        }}>
        <Text style={{color: colorsStyles.CLR_FONT, fontWeight: 'bold'}}>
          {props.txt}
        </Text>
        <Text style={{color: colorsStyles.CLR_FONT, fontWeight: 'bold'}}>
          {props.hours}
        </Text>
      </View>
    </InsetShadow>
  );
};

export default PlayWinView1;

import React from "react";
import { Dimensions, Text, View, Image } from "react-native";
import colorsStyles from "../styles/colors.styles";
import InsetShadow from "react-native-inset-shadow";
const NeuMorphicViewOfPlaWin = (props) => {
  const dimensions = {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height
  };
  const input = {
    width: dimensions.width * 0.6,
    height: dimensions.height * 0.085
  };
  return (
    <InsetShadow
      containerStyle={{
        shadowRadius: 2,
        borderRadius: 150 / 2,
        alignSelf: "center",
        shadowOpacity: 0.5,
        alignItems: "flex-start",
        justifyContent: "center",
        shadowOffset: {
          width: 3,
          height: 3
        },
        backgroundColor: colorsStyles.CLR_PRIMARY,
        width: input.width,
        height: input.height,
      }}
      elevation={10}
    >
      <View
        style={{
          //   width: '100%',
          marginLeft: 40,
          //   justifyContent: 'space-around',
          alignItems: "flex-start",
          flexDirection: "row"
        }}
      >
        <Text
          style={{
            fontSize: 14,
            color: colorsStyles.CLR_FONT
          }}
        >
          {props.txt1}
        </Text>
        <Text
          style={{
            fontSize: 14,
            color: colorsStyles.CLR_FONT_SUBTITLE,
            marginLeft: 40
          }}
        >
          {props.round}
        </Text>
      </View>
      <View
        style={{
          marginLeft: 40,
          justifyContent: "space-around",
          alignItems: "flex-start",
          flexDirection: "row"
        }}
      >
        <Text
          style={{
            fontSize: 14,
            color: colorsStyles.CLR_FONT,
            textAlign: "center"
          }}
        >
          {props.txt2}
        </Text>
        <Text
          style={{
            fontSize: 14,
            color: colorsStyles.CLR_FONT_SUBTITLE,
            textAlign: "center"
          }}
        >
          {props.prevwin}
        </Text>
      </View>
    </InsetShadow>
  );
};

export default NeuMorphicViewOfPlaWin;

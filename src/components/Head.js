import React from 'react';
import {TouchableOpacity, StyleSheet, View, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import colorsStyles from '../styles/colors.styles';
import Icon from 'react-native-vector-icons/FontAwesome';
const Header = props => {
  const navigation = useNavigation();

  return (
    <>
      <View style={Styles.head}>
        <TouchableOpacity
          style={{height: 20, width: 20, right: 80}}
          onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={20} color={colorsStyles.CLR_ORANGE} />
        </TouchableOpacity>
        <Text style={Styles.txt}>{props.title}</Text>
      </View>
      <View>
        <Text style={{color: colorsStyles.CLR_FONT}}>{props.subtitle}</Text>
      </View>
    </>
  );
};
const Styles = StyleSheet.create({
  head: {
    flexDirection: 'row',
    top: '2%',
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '100%',
  },
  txt: {
    alignSelf: 'center',
    fontWeight: 'bold',
    color: colorsStyles.CLR_FONT_TITLE,
    fontSize: 25,
  },
});
export default Header;

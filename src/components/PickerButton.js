import React, { useState } from "react";
import { TouchableOpacity, Text, View, Image } from "react-native";
import colorsStyles from "../styles/colors.styles";
// import dimensions from "../styles/dimensions";
import { Picker } from "@react-native-picker/picker";
import InsetShadow from "react-native-inset-shadow";

const PickerButton = ({ title, icon, onPress }) => {
  const [selectedValue, setSelectedValue] = useState("java");
  const dimensions = {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  };
  const input = {
    width: dimensions.width * 0.8,
    height: dimensions.height * 0.085,
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <InsetShadow
        containerStyle={{
          shadowRadius: 2,
          marginTop: 20,
          borderRadius: 150 / 2,
          shadowOpacity: 0.5,
          alignItems: "center",
          justifyContent: "center",
          shadowOffset: {
            width: 3,
            height: 3,
          },
          backgroundColor: colorsStyles.CLR_PRIMARY,
          width: input.width,
          height: input.height,
        }}
      >
        <View
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
          }}
        >
          <Picker
            selectedValue={selectedValue}
            style={{ height: 50, width: "100%", borderWidth: 1 }}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValue(itemValue)
            }
          >
            <Picker.Item
              color={colorsStyles.CLR_FONT}
              label="                            Select Game"
              value="Select Game"
            />
            <Picker.Item label="JavaScript" value="js" />
          </Picker>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
};

export default PickerButton;

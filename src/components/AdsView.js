import React from "react";
import { View, Text,Dimensions } from "react-native";
import colorsStyles from "../styles/colors.styles";
import InsetShadow from "react-native-inset-shadow";
const AdsView = () => {
  const dimensions = {
    width:  Dimensions.get('window').width,
    height:  Dimensions.get('window').height,
  };
  return (
    <InsetShadow
      containerStyle={{
        shadowRadius: 3,
        marginTop: 20,
        borderRadius: 0,
        shadowOpacity: 1,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        marginBottom: 20,
        shadowOffset: {
          width: 3,
          height: 3,
        },
        backgroundColor: colorsStyles.CLR_PRIMARY,
        width: dimensions.width * 0.925,
        height: dimensions.height * 0.21,
      }}
    >
      <View
        style={{
          width: "100%",
          height: "100%",
          //   backgroundColor: "red",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text>ads</Text>
      </View>
    </InsetShadow>
  );
};

export default AdsView;

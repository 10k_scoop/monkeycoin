import React from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import colorsStyles from "../styles/colors.styles";

const TouchableButtton = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.button}>
      <Text style={styles.text}>{props.text}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  button: {
    // height: 45,
    // width: '35%',
    padding: 40,
    paddingVertical: 10,
    marginBottom: "10%",
    alignSelf: "center",
    backgroundColor: colorsStyles.CLR_ORANGE,
    borderRadius: 150 / 2,
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
    marginHorizontal:10
  },

  text: {
    color: colorsStyles.CLR_WHITE,
    alignSelf: "center",
    fontSize: 18,
    fontWeight: "bold",
  },
});
export default TouchableButtton;

import React from "react";
import { TouchableOpacity, View, Text,Dimensions } from "react-native";
import colorsStyles from "../styles/colors.styles";
import { useNavigation } from "@react-navigation/native";
import InsetShadow from "react-native-inset-shadow";
const HomeNavBtn = ({ title, onPress, navigateTo }) => {
  const dimensions = {
    width:  Dimensions.get('window').width,
    height:  Dimensions.get('window').height,
  };
  const navigation = useNavigation();
  const navigationHandler = () => {
    if (navigateTo) navigation.navigate(navigateTo);
    else console.log("Chutti Kar");
  };
  return (
    <TouchableOpacity onPress={navigationHandler}>
      <InsetShadow
        containerStyle={{
          shadowRadius: 3,
          borderRadius: 150 / 2,
          backgroundColor: colorsStyles.CLR_BG_LIGHT,
          width: dimensions.width * 0.255,
          height: dimensions.width * 0.255,
        }}
      >
        <View
          style={{
            width: "100%",
            height: "100%",
            // backgroundColor: colorsStyles.CLR_BG_LIGHT,
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 150 / 2,
          }}
        >
          <Text
            style={{
              color: colorsStyles.CLR_FONT_TITLE,
              fontSize: title === "Notifications" ? 14 : 17,
              textAlign: "center",
            }}
          >
            {title}
          </Text>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
};

export default HomeNavBtn;

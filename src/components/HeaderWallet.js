import React from "react";
import { TouchableOpacity, StyleSheet, View, Text ,Dimensions} from "react-native";
import { useNavigation } from "@react-navigation/native";
import colorsStyles from "../styles/colors.styles";
import Icon from "react-native-vector-icons/FontAwesome";
const HeaderWallet = (props) => {
  const navigation = useNavigation();
  const dimensions = {
    width:  Dimensions.get('window').width,
    height:  Dimensions.get('window').height,
  };
  return (
    <>
      <View style={Styles.head}>
        <TouchableOpacity
          style={{ position: "absolute", left: "10%", alignSelf: "center" }}
          onPress={() => navigation.goBack()}
        >
          <Icon name="arrow-left" size={24} color={colorsStyles.CLR_ORANGE} />
        </TouchableOpacity>
        <View style={{ alignItems: "center" }}>
          <Text style={Styles.txt}>{props.title}</Text>
          {props.subtitle ? (
            <Text style={Styles.subtxt}>{props.subtitle}</Text>
          ) : null}
        </View>
      </View>
    </>
  );
};
const Styles = StyleSheet.create({
  head: {
    flexDirection: "row",
    height: "12%",
    marginTop: "4%",
    // paddingBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  txt: {
    fontWeight: "bold",
    color: colorsStyles.CLR_FONT_TITLE,
    fontSize: 25,
  },
  subtxt: {
    fontWeight: "bold",
    color: colorsStyles.CLR_FONT,
    // fontSize: 25,
  },
});
export default HeaderWallet;

import React from "react";
import { TextInput, Dimensions } from "react-native";
import colorsStyles from "../styles/colors.styles";
import InsetShadow from "react-native-inset-shadow";
const NeumorphicInput = (props) => {
  const dimensions = {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  };
  const input = {
    width: dimensions.width * 0.8,
    height: dimensions.height * 0.085,
  };
  return (
    <InsetShadow
      containerStyle={{
        shadowRadius: 2,
        marginTop: 20,
        borderRadius: 150 / 2,
        shadowOpacity: 0.5,
        shadowOffset: {
          width: 3,
          height: 3,
        },
        backgroundColor: colorsStyles.CLR_PRIMARY,
        width: input.width,
        height: input.height,
      }}
    >
      <TextInput
        placeholder={props.placeholder}
        placeholderTextColor={colorsStyles.DARK_SHADOW}
        value={props.value}
        onChangeText={props.setValue}
        keyboardType={props.keyboardType}
        editable={props.editable}
        style={{
          width: "100%",
          height: "100%",
          paddingHorizontal: 20,
          color: colorsStyles.CLR_FONT,
          paddingHorizontal: "15%",
        }}
      />
    </InsetShadow>
  );
};

export default NeumorphicInput;

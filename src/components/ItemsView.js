import React from 'react';
import {TouchableOpacity, View, Text,Dimensions} from 'react-native';
import colorsStyles from '../styles/colors.styles';
import dimensions from '../styles/dimensions';
import InsetShadow from "react-native-inset-shadow";
const ItemsView = ({children}) => {
  const dimensions = {
    width:  Dimensions.get('window').width,
    height:  Dimensions.get('window').height,
  };
  return (
    <InsetShadow
      darkShadowColor={colorsStyles.DARK_SHADOW}
      containerStyle={{
        shadowRadius: 1.5,
        borderRadius: 20,
        backgroundColor: colorsStyles.CLR_BG_LIGHT,
        // backgroundColor: 'red',
        justifyContent: 'center',
        width: dimensions.width * 0.255,
        height: dimensions.width * 0.255,
      }}>
      {children}
    </InsetShadow>
  );
};

export default ItemsView;

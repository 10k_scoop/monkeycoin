import React from "react";
import { TouchableOpacity, Text, View, Image,Dimensions  } from "react-native";
import colorsStyles from "../styles/colors.styles";
// import dimensions from "../styles/dimensions";

import InsetShadow from "react-native-inset-shadow";
const NeumorphicButton = ({ title, icon, onPress }) => {
  const dimensions = {
    width:  Dimensions.get('window').width,
    height:  Dimensions.get('window').height,
  };
  const input = {
    width: dimensions.width * 0.8,
    height: dimensions.height * 0.085,
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <InsetShadow
        containerStyle={{
          shadowRadius: 2,
          marginTop: 20,
          borderRadius: 150 / 2,
          alignSelf: "center",
          shadowOpacity: 0.5,
          alignItems: "center",
          justifyContent: "center",
          shadowOffset: {
            width: 3,
            height: 3,
          },
          backgroundColor: colorsStyles.CLR_PRIMARY,
          width: input.width,
          height: input.height,
        }}
        elevation={10}
      >
        <View
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
          }}
        >
          <Image source={icon} style={{ position: "absolute", left: "10%" }} />
          <Text
            style={{
              fontSize: 16,
              color: colorsStyles.CLR_FONT,
              textAlign: "center",
            }}
          >
            {title}
          </Text>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
};

export default NeumorphicButton;

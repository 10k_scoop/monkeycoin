import React from "react";
import dimensions from "../styles/dimensions";
import { View, Image, TouchableOpacity,Dimensions } from "react-native";
import colorsStyles from "../styles/colors.styles";
import InsetShadow from "react-native-inset-shadow";
const AppHeaderBtn = ({ icon, onPress }) => {
  const dimensions = {
    width:  Dimensions.get('window').width,
    height:  Dimensions.get('window').height,
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <InsetShadow
        containerStyle={{
          borderRadius: 150 / 2,
          shadowRadius: 5,
          //   marginTop: 20,
          borderRadius: 150 / 2,
          shadowOpacity: 0.5,
          alignItems: "center",
          justifyContent: "center",
          shadowOffset: {
            width: 3,
            height: 3,
          },
          backgroundColor: colorsStyles.CLR_PRIMARY,
          width: dimensions.width * 0.12,
          height: dimensions.width * 0.12,
        }}
      >
        <View
          style={{
            width: "100%",
            height: "100%",
            backgroundColor: colorsStyles.CLR_HEADER_BTN,
            borderRadius: 150 / 2,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Image source={icon} />
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
};

export default AppHeaderBtn;

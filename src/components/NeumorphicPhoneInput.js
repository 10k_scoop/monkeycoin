import React from "react";
import PhoneInput from "react-native-phone-number-input";

const NeumorphicPhoneInput = () => {
  return (
    <PhoneInput
      ref={phoneInput}
      defaultValue={value}
      defaultCode="DM"
      layout="first"
      onChangeText={(text) => {
        setValue(text);
      }}
      onChangeFormattedText={(text) => {
        setFormattedValue(text);
      }}
      withDarkTheme
      withShadow
      autoFocus
    />
  );
};

export default NeumorphicPhoneInput;

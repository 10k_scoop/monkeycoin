import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import colorsStyles from "../styles/colors.styles";
import dimensions from "../styles/dimensions";
import ItemsView from "./ItemsView";
import Icon1 from "react-native-vector-icons/FontAwesome";
import InsetShadow from "react-native-inset-shadow";
const PlayWinRoundView = (props) => {
  const [count, setCount] = React.useState(0);
  const [selectedAmount, setSelectedAmount] = useState(null);
  return (
    <>
      <InsetShadow
        containerStyle={{
          shadowRadius: 3,
          marginTop: 20,
          borderRadius: 20,
          shadowOpacity: 1,
          alignItems: "center",
          justifyContent: "center",
          alignSelf: "center",
          marginBottom: 20,
          shadowOffset: {
            width: 3,
            height: 3,
          },
          backgroundColor: colorsStyles.CLR_PRIMARY,
          width: dimensions.windowWidth * 0.857,
          height: dimensions.windowHeight * 0.28,
        }}
      >
        <View
          style={{
            width: "100%",
            height: "100%",
            //   backgroundColor: 'red',
            //   justifyContent: 'center',
            alignItems: "center",
          }}
        >
          <Text
            style={{
              marginTop: 10,
              fontWeight: "bold",
              fontSize: 17,
              color: colorsStyles.CLR_FONT_TITLE,
            }}
          >
            {props.round}
          </Text>
          <Text style={{ color: colorsStyles.CLR_FONT }}>{props.win}</Text>
          <View
            style={{
              //   backgroundColor: '#000',
              height: 100,
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <TouchableOpacity
              onPress={() => {
                setSelectedAmount(100);
              }}
            >
              <ItemsView>
                <Image
                  style={styles.img}
                  source={require("../assets/images/burgr.png")}
                />
                <Text style={styles.txt}>100</Text>
                <View
                  style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: dimensions.windowWidth * 0.255,
                    height: dimensions.windowWidth * 0.255,
                    // backgroundColor: 'red',
                    borderRadius: 20,
                    borderColor: colorsStyles.CLR_ORANGE,
                    borderWidth: selectedAmount == 100 ? 3 : 0,
                  }}
                ></View>
              </ItemsView>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setSelectedAmount(200);
              }}
            >
              <ItemsView>
                <Image
                  style={styles.img}
                  source={require("../assets/images/pizza.png")}
                />
                <Text style={styles.txt}>200</Text>
                <View
                  style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: dimensions.windowWidth * 0.255,
                    height: dimensions.windowWidth * 0.255,
                    // backgroundColor: 'red',
                    borderRadius: 20,
                    borderColor: colorsStyles.CLR_ORANGE,
                    borderWidth: selectedAmount == 200 ? 3 : 0,
                  }}
                ></View>
              </ItemsView>
            </TouchableOpacity>
          </View>
          <View
            style={{
              //   backgroundColor: '#000',
              height: 40,
              width: "100%",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <TouchableOpacity
              style={{ right: 10 }}
              onPress={() => {
                if (count == 0) {
                  return null;
                } else setCount(count - 1);
              }}
            >
              <Icon1
                name="chevron-down"
                size={20}
                color={colorsStyles.CLR_FONT_TITLE}
              />
            </TouchableOpacity>
            <InsetShadow
              containerStyle={{
                shadowRadius: 3,
                borderRadius: 10,
                shadowOpacity: 1,
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center",
                // marginBottom: 20,
                shadowOffset: {
                  width: 3,
                  height: 3,
                },
                backgroundColor: colorsStyles.CLR_PRIMARY,
                width: dimensions.windowWidth * 0.13,
                height: dimensions.windowHeight * 0.05,
              }}
            >
              <Text>{count}</Text>
            </InsetShadow>
            <TouchableOpacity
              style={{ left: 10 }}
              onPress={() => setCount(count + 1)}
            >
              <Icon1
                name="chevron-up"
                size={20}
                color={colorsStyles.CLR_FONT_TITLE}
              />
            </TouchableOpacity>
          </View>
        </View>
      </InsetShadow>
    </>
  );
};
const styles = StyleSheet.create({
  img: {
    alignSelf: "center",
  },
  txt: {
    alignSelf: "center",
    color: colorsStyles.CLR_FONT,
  },
});
export default PlayWinRoundView;

import React from "react";
import { View, Text, Image ,Dimensions} from "react-native";
import colorsStyles from "../styles/colors.styles";
import InsetShadow from "react-native-inset-shadow";
const MonkeyCoinView = (props) => {
  const dimensions = {
    width:  Dimensions.get('window').width,
    height:  Dimensions.get('window').height,
  };
  return (
    <InsetShadow
      containerStyle={{
        shadowRadius: 3,
        marginTop: 20,
        borderRadius: 10,
        shadowOpacity: 1,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        // marginBottom: 20,
        shadowOffset: {
          width: 3,
          height: 3,
        },
        backgroundColor: colorsStyles.CLR_PRIMARY,
        width: dimensions.width * 0.8,
        height: dimensions.height * 0.24,
      }}
    >
      <View
        style={{
          width: "100%",
          height: "100%",
          //   backgroundColor: 'red',
          //   justifyContent: 'center',
          alignItems: "center",
        }}
      >
        <Text
          style={{
            color: colorsStyles.CLR_ORANGE,
            fontWeight: "bold",
            fontSize: 12,
            marginTop: "5%",
          }}
        >
          Your Monkey Coin
        </Text>
        <View
          style={{
            flexDirection: "row",
            // backgroundColor: '#000',
            // height: "80%",
            width: "100%",
            marginTop: "10%",
            // bottom: "8%",
            justifyContent: "space-around",
            alignItems: "flex-end",
          }}
        >
          <Image
            source={require("../assets/images/monkeycoin.png")}
            // style={{marginLeft: '4%'}}
          />
          <View
            style={{
              height: "82%",
              width: "30%",
              backgroundColor: colorsStyles.CLR_ORANGE,
              borderRadius: 8,
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                alignSelf: "center",
                fontSize: 22,
                fontWeight: "bold",
                color: colorsStyles.CLR_WHITE,
              }}
            >
              {props.coincount} MC
            </Text>
          </View>
        </View>
      </View>
    </InsetShadow>
  );
};

export default MonkeyCoinView;

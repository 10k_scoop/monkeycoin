import React from "react";
import { Dimensions, Text, View, Image } from "react-native";
import colorsStyles from "../styles/colors.styles";
import InsetShadow from "react-native-inset-shadow";
const NeuMorphicView = (props) => {
  const dimensions = {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  };
  const input = {
    width: dimensions.width * 0.8,
    height: dimensions.height * 0.085,
  };
  return (
    <InsetShadow
      containerStyle={{
        shadowRadius: 2,
        marginTop: 20,
        borderRadius: 150 / 2,
        shadowOpacity: 0.5,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        alignSelf: "center",
        shadowOffset: {
          width: 3,
          height: 3,
        },
        backgroundColor: colorsStyles.CLR_PRIMARY,
        width: input.width,
        height: input.height,
      }}
    >
      <View
        style={{
          width: "50%",
          height: "80%",
          //   backgroundColor: '#000',
          flexDirection: "row",
        }}
      >
        <View
          style={{
            width: "16%",
            height: "50%",
            left: 10,
            top: 15,
            // backgroundColor: '#fff',
          }}
        >
          <Text
            style={{
              fontSize: 16,
              color: colorsStyles.CLR_WHITE,
              backgroundColor: colorsStyles.CLR_FONT_TITLE,
              textAlign: "center",
              //   width: '7%',
              borderRadius: 25,
              // left: -80,
            }}
          >
            {props.count}
          </Text>
        </View>
        <View
          style={{
            width: "100%",
            height: "100%",
            left: 20,
            // top: 15,
            // backgroundColor: '#fff',
            // flexDirection: 'row',
          }}
        >
          <Text
            style={{
              fontSize: 11,
              color: colorsStyles.CLR_FONT,
              // top: '-3%',
              // left: -70,
            }}
          >
            {props.text}
          </Text>
          <View
            style={{
              flexDirection: "row",
              height: "70%",
              width: "70%",
              //   backgroundColor: '#000',
            }}
          >
            <Image
              source={require("../assets/images/flag.png")}
              style={{
                bottom: -9,
              }}
            />
            <Text
              style={{
                bottom: -17,
                fontSize: 12,
                color: colorsStyles.CLR_FONT,
              }}
            >
              {props.number}
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "50%",
          height: "80%",
          //   backgroundColor: '#fcfc',
        }}
      >
        <Text
          style={{
            fontSize: 11,
            color: colorsStyles.CLR_FONT_TITLE,
            fontWeight: "bold",
            left: 60,
          }}
        >
          {props.price}
        </Text>
        <Text
          style={{
            // position: 'absolute',
            top: 15,
            // right: 30,
            left: 60,
            fontSize: 12,
            color: colorsStyles.CLR_FONT,
          }}
        >
          {props.desc}
        </Text>
      </View>
    </InsetShadow>
  );
};

export default NeuMorphicView;

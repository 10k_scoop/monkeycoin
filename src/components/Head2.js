import React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import colorsStyles from "../styles/colors.styles";
import Icon from "react-native-vector-icons/FontAwesome";
import NoAds from "../assets/images/NoAds.png";
import AppHeaderBtn from "../components/AppHeaderBtn";
import UserAvatar from "react-native-user-avatar";

const Head2 = (props) => {
  const navigation = useNavigation();
  return (
    <>
      <View style={Styles.head}>
        <TouchableOpacity
          style={{ height: 20, width: 20, top: "-5%" }}
          onPress={() => navigation.goBack()}
        >
          <Icon name="arrow-left" size={20} color={colorsStyles.CLR_ORANGE} />
        </TouchableOpacity>
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <UserAvatar
            size={100}
            name={props.name}
            bgColors={["#ccc", "#fafafa", "#ccaabb"]}
          />

          <Text
            style={{
              fontWeight: "bold",
              color: colorsStyles.CLR_FONT_TITLE,
              fontSize: 15,
              // left: -7,
            }}
          >
            {props.name}
          </Text>
          <Text
            style={{
              fontWeight: "bold",
              color: colorsStyles.CLR_FONT,
              fontSize: 10,
              // left: -7,
            }}
          >
            {props.score}
          </Text>
        </View>
        <View style={{ top: "-5%" }}>
          <AppHeaderBtn icon={NoAds} />
        </View>
      </View>
    </>
  );
};
const Styles = StyleSheet.create({
  head: {
    backgroundColor: colorsStyles.CLR_PRIMARY,
    flexDirection: "row",
    paddingTop: "8.75%",
    paddingHorizontal: "7.5%",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    // height: 60,
  },
  txt: {
    // alignSelf: 'center',
    fontWeight: "bold",
    color: colorsStyles.CLR_FONT_TITLE,
    fontSize: 25,
  },
});
export default Head2;

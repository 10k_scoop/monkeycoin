import React from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import colorsStyles from "../styles/colors.styles";
// import dimensions from "../styles/dimensions";

import commentImg from "../assets/images/comment.png";
import { MaterialIcons } from "@expo/vector-icons";
import InsetShadow from "react-native-inset-shadow";
const PostFeedScreenButton = ({ win, name, round }) => {
  const dimensions = {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  };
  const input = {
    width: dimensions.width * 0.8,
    height: dimensions.height * 0.085,
  };

  return (
    <InsetShadow
      containerStyle={{
        shadowRadius: 2,
        marginTop: 20,
        borderRadius: 24,
        shadowOpacity: 0.5,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        alignSelf: "center",
        shadowOffset: {
          width: 3,
          height: 3,
        },
        backgroundColor: colorsStyles.CLR_PRIMARY,
        width: input.width,
        height: input.height,
      }}
    >
      <View style={styles.container}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View>
            <View style={{ marginBottom: 5 }}>
              <Text style={{ color: colorsStyles.CLR_FONT }}>{name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Image source={require("../assets/images/unitedkingdom.png")} />
              <Text style={{ color: colorsStyles.CLR_FONT, marginLeft: 3 }}>
                78693
              </Text>
            </View>
            <Text style={{ color: colorsStyles.CLR_FONT, fontSize: 9 }}>
              Just now
            </Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <View style={{ marginBottom: 5 }}>
              <Text
                style={{ color: "#354c79", fontWeight: "bold", fontSize: 16 }}
              >
                Win ${win}{" "}
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ color: colorsStyles.CLR_FONT, marginLeft: 3 }}>
                Round {round}
              </Text>
            </View>
          </View>
        </View>
        {/*  */}
        <View style={{ top: "16%" }}>
          <Text style={{ fontSize: 12, color: colorsStyles.CLR_FONT }}>
            jabdjbdadabdlkblkdbjabjadbdlkbdasnkldalksaandandlkndandakdnkdan;adsasadadadafsfdkan;kndlkndandknd
          </Text>
        </View>
        <View
          style={{
            top: "22%",
            // alignSelf: "flex-end",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View style={{ alignItems: "center" }}>
            <MaterialIcons
              name="favorite"
              size={32}
              color={colorsStyles.CLR_HEART}
            />
            <Text style={{ color: colorsStyles.CLR_HEART }}>1.2k</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Image source={commentImg} />
            <Text style={{ color: colorsStyles.DARK_SHADOW }}>200</Text>
          </View>
        </View>
      </View>
    </InsetShadow>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: "5%",
    paddingHorizontal: 40,
    // paddingVertical: 20,
    height: "100%",
    width: "100%",
  },
});

export default PostFeedScreenButton;

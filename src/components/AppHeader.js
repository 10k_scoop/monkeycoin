import { Text } from "react-native";
import React from "react";
import { View, Image } from "react-native";
import colorsStyles from "../styles/colors.styles";
import monkeyLogo from "../assets/images/monkeyHeaderLogo.png";
import NoAds from "../assets/images/NoAds.png";
import settingsIcon from "../assets/images/settingsIcon.png";

import { useNavigation } from "@react-navigation/native";
import AppHeaderBtn from "./AppHeaderBtn";

const AppHeader = () => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        backgroundColor: colorsStyles.CLR_PRIMARY,
        flexDirection: "row",
        paddingTop: "8.75%",
        paddingHorizontal: "7.5%",
        justifyContent: "space-between",
      }}
    >
      <View style={{}}>
        <Image
          source={monkeyLogo}
          style={
            {
              //   left: "20%",
              // width: "100%",
              // height: "100%",
            }
          }
        />
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View style={{ marginRight: 12 }}>
          <AppHeaderBtn icon={NoAds} />
        </View>
        <AppHeaderBtn
          icon={settingsIcon}
          onPress={() => {
            navigation.navigate("UpdateUser");
          }}
        />
      </View>
    </View>
  );
};

export default AppHeader;

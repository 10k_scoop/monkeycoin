import React from "react";
import { useEffect } from "react";
import { Text, View } from "react-native";
import colorsStyles from "../styles/colors.styles";

const LoadingScreen = (props) => {
  const { navigation } = props;
  console.log("LoadingScreen");
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate("LoginScreen");
    }, 2000);
  }, []);
  return (
    <View
      style={{
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        flex:1,
      }}
    >
      <Text>Loading Screen</Text>
    </View>
  );
};

export default LoadingScreen;

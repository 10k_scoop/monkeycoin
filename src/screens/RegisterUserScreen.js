import React, { useState } from "react";
import colorsStyles from "../styles/colors.styles";
import NeumorphicInput from "../components/NeumorphicInput";
import TouchableButton from "../components/TouchableButton";
import { View, ScrollView, StyleSheet, Image, StatusBar } from "react-native";
import MonkeyLogo from "../assets/images/monkeycoin.png";
import TreesImg from "../assets/images/registerUserTreesImg.png";
const RegisterUSerScreen = ({ navigation }) => {
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [phone, setphone] = useState("");
  const [language, setlanguage] = useState("");
  const [referncenumber, setreferncenumber] = useState("");
  
  return (
    <View style={{ backgroundColor: colorsStyles.CLR_PRIMARY, paddingTop:StatusBar.currentHeight+10 }}>
      <View style={{ alignItems: "center" }}>
        <Image source={MonkeyLogo} style={{ top: "5%" }} />
        <Image source={TreesImg} />
      </View>
      <View style={styles.container}>
        <NeumorphicInput
          placeholder="Full Name"
          value={name}
          setValue={setname}
          keyboardType="default"
        />
        <NeumorphicInput
          placeholder="Email"
          value={email}
          setValue={setemail}
        />
        <NeumorphicInput
          placeholder="Phone Number"
          value={phone}
          setValue={setphone}
          keyboardType="numeric"
        />
        <NeumorphicInput
          placeholder="Languages/لغة"
          value={language}
          setValue={setlanguage}
          keyboardType="default"
        />
        <NeumorphicInput
          placeholder="Refernce Number (Optional)"
          value={referncenumber}
          setValue={setreferncenumber}
          keyboardType="default"
        />
        <View style={{ marginTop: 20 }}>
          <TouchableButton
            text="Next"
            onPress={() => {
              navigation.navigate("LoginScreen");
            }}
          />
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    // flex: 1,
    // justifyContent: 'center',
    alignItems: "center",
    backgroundColor: colorsStyles.CLR_PRIMARY,
  },
});
export default RegisterUSerScreen;

import React from "react";
import { Image, Text, View } from "react-native";
import NeumorphicButton from "../components/NeumorphicButton";
import NeumorphicInput from "../components/NeumorphicInput";
import colorsStyles from "../styles/colors.styles";
import dimensions from "../styles/dimensions";
import facebookLogo from "../assets/images/facebook.png";
import googleLogo from "../assets/images/google.png";
import monkeyLogo from "../assets/images/monkeycoin.png";
import treesLogo from "../assets/images/trees.png";
import AdsView from "../components/AdsView";
const WalletScreen = (props) => {
  return (
    <View
      style={{
        height: "100%",
        paddingHorizontal: 20,
        // justifyContent: "center",
        alignItems: "center",
        backgroundColor: colorsStyles.CLR_PRIMARY,
      }}
    >
      <NeumorphicButton title={"Purchase Coin"} onPress={null} />
      <NeumorphicButton title={"Spin & Win"} onPress={null} />

      <AdsView />
    </View>
  );
};

export default WalletScreen;

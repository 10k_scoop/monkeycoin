import React, { useState, useEffect } from "react";
import { Image, Text, View } from "react-native";
import NeumorphicButton from "../components/NeumorphicButton";
import colorsStyles from "../styles/colors.styles";
// import dimensions from "../styles/dimensions";
import facebookLogo from "../assets/images/facebook.png";
import googleLogo from "../assets/images/google.png";
import monkeyLogo from "../assets/images/monkeycoin.png";
import treesLogo from "../assets/images/trees.png";
import { loginWithFacebook,loginWithGoogle } from "../socialLoginActions";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
const LoginScreen = (props) => {
  const dimensions = {
    width: wp('100%'),
    height:hp('100%'),
  };

  const { navigation } = props;
  return (
    <View
      style={{
        height: "100%",
        paddingHorizontal: 20,
        // justifyContent: "center",
        alignItems: "center",
        backgroundColor: colorsStyles.CLR_PRIMARY
      }}
    >
      <View style={{ top: "5%" }}>
        <Image
          source={monkeyLogo}
          style={{ top: "10%", alignSelf: "center" }}
        />
        <Image source={treesLogo} style={{ left: "10%" }} />
      </View>
      <View
        style={{
          width: dimensions.width * 0.8,

          top: "10%"
        }}
      >
        <View style={{ alignSelf: "flex-start" }}>
          <Text
            style={{
              color: colorsStyles.CLR_FONT_TITLE,
              fontSize: 40,
              fontWeight: "bold"
            }}
          >
            Monkey
          </Text>
          <Text
            style={{
              color: colorsStyles.CLR_FONT_SUBTITLE,
              fontWeight: "bold",
              fontSize: 28
            }}
          >
            Coin
          </Text>
        </View>
        <NeumorphicButton
          title={"Continue with Google"}
          icon={googleLogo}
          onPress={() => loginWithGoogle()}
        />
        <NeumorphicButton
          title={"Continue with FaceBook"}
          icon={facebookLogo}
          onPress={() => loginWithFacebook()}
        />
      </View>
      <View style={{ position: "absolute", top: "86%", width: 266 }}>
        <Text
          style={{
            fontSize: 13,
            color: colorsStyles.CLR_FONT,
            textAlign: "center"
          }}
        >
          You agree to our Term, Data Policy and Cookie Policy. Information from
          your address book will be continuously uploaded
        </Text>
      </View>
    </View>
  );
};

export default LoginScreen;

import React, { useState } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  Picker,
  ScrollView,
} from "react-native";
import colorsStyles from "../styles/colors.styles";
import NeumorphicButton from "../components/NeumorphicButton";

import treesLogo from "../assets/images/trees.png";
import HeaderWallet from "../components/HeaderWallet";
import TouchableButtton from "../components/TouchableButton";
const PurchaseCoinScreen = (props) => {
  const [selectedValue, setSelectedValue] = useState("java");
  console.log("PurchaseCoinScreen");
  return (
    <View style={styles.container}>
      <HeaderWallet title={"Purchase Coins"} />
      <ScrollView>
        <Image source={treesLogo} style={{ alignSelf: "center" }} />
        <View style={styles.headingStylingView}>
          {/* <Text style={styles.headingStyling}>Purchase Coin</Text> */}
        </View>

        <View style={{ alignItems: "center" }}>
          <View>
            <NeumorphicButton
              title="2500 coins = $5"
              colorName="black"
              fontWeight="bold"
            />
          </View>
          <View>
            <NeumorphicButton
              title="6000 coins = $10"
              colorName="black"
              fontWeight="bold"
            />
          </View>
          <View>
            <NeumorphicButton
              title="33000 coins = $50"
              colorName="black"
              fontWeight="bold"
            />
          </View>
          <View>
            <NeumorphicButton
              title="70000 coins = $100"
              colorName="black"
              fontWeight="bold"
            />
          </View>
        </View>

        <View style={styles.footerStylingView}>
          <TouchableButtton text="Purchase" />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",

    backgroundColor: colorsStyles.CLR_PRIMARY,
  },
  HeaderStylingView: {
    height: "37%",
  },
  headingStyling: {
    fontSize: 25,
    fontWeight: "bold",
  },
  headingStylingView: {
    alignItems: "center",
  },
  buttonStylingView: {
    alignItems: "center",
  },
  pickerStylingView: {
    alignItems: "center",
    borderWidth: 1,
    marginHorizontal: 40,
    borderRadius: 120,
    marginTop: 15,
    fontWeight: "bold",
  },
  AdStylingView: {
    marginTop: "3%",
    height: "32%",
    marginHorizontal: 12,
    //backgroundColor:'yellow'
  },
  footerStyling: {
    fontWeight: "bold",
    fontSize: 18,
    color: "white",
  },
  footerStylingView: {
    marginTop: "10%",
  },
});

export default PurchaseCoinScreen;

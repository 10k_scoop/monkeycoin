import React, { useState } from "react";
import colorsStyles from "../styles/colors.styles";
import NeumorphicInput from "../components/NeumorphicInput";
import TouchableButton from "../components/TouchableButton";
import Head2 from "../components/Head2";
import AdsView from "../components/AdsView";
const firebase = require("firebase");
import {
  View,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
const UpdateUser = () => {
  const [nameUpdate, setNameUpdate] = useState("");
  const [gmail, setGmail] = useState("");
  const [userIdNum, setUserIdNum] = useState("");
  const [refaleNum, setRefaleNum] = useState("");
  console.log("UpdateUserScreen");

  const logout=()=>{
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
      
    }).catch((error) => {
      // An error happened.
    });  
  }
  return (
    <>
      <ScrollView>
        <View style={styles.container}>
          <Head2 name="Emon" score="79865" />
          <NeumorphicInput
            placeholder="Name"
            value={nameUpdate}
            setValue={setNameUpdate}
            keyboardType="default"
          />
          <NeumorphicInput
            placeholder="Gmail"
            value={gmail}
            setValue={setGmail}
          />
          <NeumorphicInput
            placeholder="User ID Num"
            value={userIdNum}
            setValue={setUserIdNum}
            keyboardType="numeric"
            editable={false}
          />
          <NeumorphicInput
            placeholder="Reffale Num"
            value={refaleNum}
            setValue={setRefaleNum}
            keyboardType="default"
            editable={false}
          />
          <AdsView />
          <View style={{flexDirection: "row"}}>
            <TouchableButton text="Update" />
            <TouchableButton text="Logout" onPress={logout} />
          </View>
          <View style={styles.contactview}>
            <Text style={styles.contacttext}>Contact Us</Text>
            <TouchableOpacity>
              <Text
                style={{ color: colorsStyles.CLR_ORANGE, marginBottom: 40 }}
              >
                Admin@monkeycoin.live
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    height: "100%",
    // flex: 1,
    // justifyContent: 'center',
    alignItems: "center",
    backgroundColor: colorsStyles.CLR_PRIMARY,
  },
  contactview: {
    top: 67,
    flexDirection: "row",
    // justifyContent: 's',
  },
  contacttext: {
    marginHorizontal: "3%",
    color: colorsStyles.CLR_FONT_SUBTITLE,
    fontWeight: "bold",
  },
});
export default UpdateUser;

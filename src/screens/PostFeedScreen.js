import React from "react";
import {
  Image,
  Text,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
} from "react-native";
import NeumorphicButton from "../components/NeumorphicButton";
import NeumorphicInput from "../components/NeumorphicInput";
import colorsStyles from "../styles/colors.styles";
import dimensions from "../styles/dimensions";
import facebookLogo from "../assets/images/facebook.png";
import googleLogo from "../assets/images/google.png";
import monkeyLogo from "../assets/images/monkeycoin.png";
import treesLogo from "../assets/images/trees.png";
import PostFeedScreenButton from "../components/PostFeedScreenButton";
import AdsView from "../components/AdsView";
import { useCallback } from "react";
import { useWindowDimensions } from "react-native";
import HeaderWallet from "../components/HeaderWallet";

const PostFeedScreen = (props) => {
  const dimensions = {
    width: useWindowDimensions().width,
    height: useWindowDimensions().height,
  };
  const feeds = [
    { name: "John Smith", win: 200, round: 23 },
    { name: "Smith", win: 200, round: 23 },
    { name: "John Wick", win: 200, round: 23 },
    { name: "Jack Reacher", win: 200, round: 23 },
    { name: "Jack Sparrow", win: 200, round: 23 },
    { name: "Clank", win: 200, round: 23 },
  ];
  console.log("PostFeedScreen");
  const renderItem = useCallback(({ item, index }) => {
    return (
      <>
        {index == 1 ? (
          <>
            <PostFeedScreenButton
              win={item.win}
              name={item.name}
              round={item.round}
            />
            <AdsView />
          </>
        ) : (
          <PostFeedScreenButton
            win={item.win}
            name={item.name}
            round={item.round}
          />
        )}
      </>
    );
  });

  return (
    <>
      <View style={Styles.container}>
        {/* <Head title="Winner List" subtitle="Last 7 Days"/> */}
        <HeaderWallet title="Post Feed" subtitle="Last 7 Days" />
        <View style={{ height: "82%", paddingBottom: 20 }}>
          <FlatList
            data={feeds}
            renderItem={renderItem}
            keyExtractor={(item) => item.name}
          />
        </View>
      </View>
    </>
  );
};
const Styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: colorsStyles.CLR_PRIMARY,
    height: "100%",
  },
  head: {
    flexDirection: "row",

    justifyContent: "center",
    width: "100%",
  },
  txt: {
    alignSelf: "center",
    fontWeight: "bold",
    color: colorsStyles.CLR_FONT_TITLE,
    fontSize: 25,
  },
});

export default PostFeedScreen;

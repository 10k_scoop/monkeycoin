import React from "react";
import NeuMorphicView from "../components/NeuMorphicView";
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  TouchableOpacity,
} from "react-native";
import colorsStyles from "../styles/colors.styles";
import Head from "../components/Head";
import AdsView from "../components/AdsView";
import HeaderWallet from "../components/HeaderWallet";
const WinnerList = () => {
  const Count = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  console.log("WinnerScreen");
  return (
    <>
      <View style={Styles.container}>
        {/* <Head title="Winner List" subtitle="Last 7 Days"/> */}
        <HeaderWallet title="Winner List" subtitle="Last 7 Days" />
        <View style={{ height: "82%", paddingBottom: 20 }}>
          <FlatList
            // style={{padding: 20}}
            contentContainerStyle={{
              justifyContent: "center",
              alignItems: "center",
            }}
            data={Count}
            keyExtractor={(item) => item}
            renderItem={({ item, index }) => (
              <>
                <NeuMorphicView
                  count={item}
                  text="John Smith"
                  price="Win $100"
                  number="76893"
                  desc="Round 43"
                />
                {index == 2 ? <AdsView /> : null}
              </>
            )}
          />
        </View>
      </View>
    </>
  );
};
const Styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: colorsStyles.CLR_PRIMARY,
    height: "100%",
  },
  head: {
    flexDirection: "row",

    justifyContent: "center",
    width: "100%",
  },
  txt: {
    alignSelf: "center",
    fontWeight: "bold",
    color: colorsStyles.CLR_FONT_TITLE,
    fontSize: 25,
  },
});
export default WinnerList;

import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import colorsStyles from '../styles/colors.styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import HeaderWallet from '../components/HeaderWallet';
import MonkeyCoinView from '../components/MonkeyCoinView';
import NeumorphicButton from '../components/NeumorphicButton';
import AdsView from '../components/AdsView';
import TouchableButtton from '../components/TouchableButton';
const Wallet = ({navigation}) => {
  //   const navigation = useNavigation();
  console.log("WalletScreen");
  return (
    <>
      <View style={Styles.container}>
        <HeaderWallet title="Wallet" />
        <View style={Styles.main}>
          <MonkeyCoinView coincount="75" />
          <NeumorphicButton
            title="Purchase Coin"
            onPress={() => {
              navigation.navigate('PurchaseCoinScreen');
            }}
          />
          <NeumorphicButton title="Spin & Win" />
          <AdsView />
          <TouchableButtton
            text="Exchange Coins"
            onPress={() => {
              navigation.navigate('ExchangeCoinsScreen');
            }}
          />
        </View>
      </View>
    </>
  );
};
const Styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: colorsStyles.CLR_PRIMARY,
  },
  main: {
    backgroundColor: colorsStyles.CLR_PRIMARY,
    height: '100%',
    // alignItems: "center",
  },
  txt: {},
});
export default Wallet;

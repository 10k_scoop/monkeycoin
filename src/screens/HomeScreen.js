import React from "react";
import { View, Text, Image } from "react-native";
import colorsStyles from "../styles/colors.styles";
import monkeyLogo from "../assets/images/monkeycoin.png";
import AppHeader from "../components/AppHeader";
import treesLogo from "../assets/images/trees.png";
import HomeNavBtn from "../components/HomeNavBtn";
// import dimensions from "../styles/dimensions";
import { ScrollView } from "react-native";
import AdsView from "../components/AdsView";

const HomeScreen = ({ navigation }) => {
  console.log("HomeScreen");
  return (
    <ScrollView
      style={{
        minHeight: "100%",
        backgroundColor: colorsStyles.CLR_PRIMARY,

      }}
    >
      <View>
        {/* <Image source={{ monkeyLogo }} />
      <Text>Home</Text> */}
        <AppHeader />
        <Image source={require("../assets/images/trees.png")} style={{ left: "10%", top: "2%" }} />
        <View
          style={{
            padding: "7.5%",
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          <HomeNavBtn title={`Play\n&\nWin`} navigateTo="Playandwin" />
          <View style={{ width: 15, height: 2 }}></View>
          <HomeNavBtn title={`Winner\nList`} navigateTo="WinnerList" />
          <View style={{ width: 15, height: 2 }}></View>
          <HomeNavBtn title={`Wallet`} navigateTo="Wallet" />
          <View style={{ width: "100%", height: 15 }}></View>

          <HomeNavBtn title={`Post\nFeed`} navigateTo="PostFeedScreen" />
          <View style={{ width: 15, height: 2 }}></View>
          <HomeNavBtn title={`User\nGuide`} navigateTo="PurchaseCoinScreen" />
          <View style={{ width: 15, height: 2 }}></View>
          <HomeNavBtn title={`Notifications`} />
        </View>
      </View>
      <AdsView />
    </ScrollView>
  );
};

export default HomeScreen;

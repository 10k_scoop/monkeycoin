import React, { useState } from "react";
import { Text, View, StyleSheet, ScrollView } from "react-native";
import colorsStyles from "../styles/colors.styles";
import NeumorphicButton from "../components/NeumorphicButton";
import { BorderlessButton } from "react-native-gesture-handler";
import AdsView from "../components/AdsView";
import { Picker } from "@react-native-picker/picker";
import TouchableButtton from "../components/TouchableButton";
import PickerButton from "../components/PickerButton";
import HeaderWallet from "../components/HeaderWallet";

const ExchangeCoinsScreen = (props) => {
  const [selectedValue, setSelectedValue] = useState("java");
  console.log("ExchangeCoinsScreen");
  return (
    <View style={styles.container}>
      {/* <View style={styles.headingStylingView}>
        <Text style={styles.headingStyling}>
          Exchange Coins
        </Text>
      </View> */}
      <HeaderWallet title="Exchange Coins" />
      <ScrollView>
        <View style={{ alignItems: "center" }}>
          <PickerButton />
        </View>
        <View style={styles.buttonStylingView}>
          <View>
            <NeumorphicButton
              title="200 coins = 7 UC"
              colorName="black"
              fontWeight="bold"
            />
          </View>
          <View>
            <NeumorphicButton
              title="1000 coins = 74 UC"
              colorName="black"
              fontWeight="bold"
            />
          </View>
          <View>
            <NeumorphicButton
              title="4000 coins = 294 UC"
              colorName="black"
              fontWeight="bold"
            />
          </View>
          <View>
            <NeumorphicButton
              title="10000 coins = 782 UC"
              colorName="black"
              fontWeight="bold"
            />
          </View>
        </View>

        <AdsView />

        <View style={{ height: 100, alignItems: "center" }}>
          <TouchableButtton text="Exchange" />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: colorsStyles.CLR_PRIMARY,
  },
  headingStyling: {
    fontSize: 30,
    fontWeight: "bold",
  },
  headingStylingView: {
    alignItems: "center",
  },
  buttonStylingView: {
    alignItems: "center",
  },
  pickerStylingView: {
    alignItems: "center",
    borderWidth: 1,
    marginHorizontal: 40,
    borderRadius: 120,
    marginTop: 15,
    fontWeight: "bold",
  },
  AdStylingView: {
    marginTop: "3%",
    height: "32%",
    marginHorizontal: 12,
    //backgroundColor:'yellow'
  },
  footerStyling: {
    fontWeight: "bold",
    fontSize: 18,
    color: "white",
  },
  footerStylingView: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "orange",
    height: 50,
    marginHorizontal: "35%",
    borderRadius: 120,
  },
});

export default ExchangeCoinsScreen;

import React from 'react';
import {Text, View, StyleSheet, StatusBar} from 'react-native';
import AdsView from '../components/AdsView';
import NeumorphicButton from '../components/NeumorphicButton';
import NeuMorphicView from '../components/NeuMorphicView';
import NeuMorphicViewOfPlaWin from '../components/NeuMorphicViewOfPlaWin';
import PlayWinHeader from '../components/PlayWinHeader';
import PlayWinRoundView from '../components/PlayWinRoundView';
import PlayWinView from '../components/PlayWinView';
import PlayWinView1 from '../components/PlayWinView1';
import TouchableButtonOfPlayWin from '../components/TouchableButtonOfPlayWin';
import colorsStyles from '../styles/colors.styles';
const Playandwin = () => {
  return (
    <View style={styles.container}>
      <PlayWinHeader title="Play & Win" />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
        }}>
        <PlayWinView txt="coins-100" />
        <PlayWinView1 txt="Round End" hours="2 Hours" />
      </View>
      <PlayWinRoundView round="Round-1" win="Win 2X" />
      <View
        style={{
          //   height: 100,
          //   width: '100%',
          //   backgroundColor: '#000',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <NeuMorphicViewOfPlaWin
          txt1="Round"
          round="4"
          txt2="Previous Win"
          prevwin="100 MC"
        />
        <TouchableButtonOfPlayWin text="Play" />
      </View>
      <AdsView />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: colorsStyles.CLR_PRIMARY,
    paddingTop: StatusBar.currentHeight + 10
  },
});
export default Playandwin;

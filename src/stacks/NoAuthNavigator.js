import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoadingScreen from "../screens/LoadingScreen";
import LoginScreen from "../screens/LoginScreen";
import RegisterUSerScreen from "../screens/RegisterUserScreen";


const { Navigator, Screen } = createStackNavigator();

const RootStack = () => {
  return (
    <Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
    <Screen name="RegisterUSerScreen" component={RegisterUSerScreen} />
      <Screen name="LoadingScreen" component={LoadingScreen} />
      <Screen name="LoginScreen" component={LoginScreen} />

    </Navigator>
  );
};

export const NoAuthNavigator = () => (
  <NavigationContainer>
    <RootStack />
  </NavigationContainer>
);
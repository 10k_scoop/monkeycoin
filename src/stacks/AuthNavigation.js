import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "../screens/HomeScreen";
import UpdateUser from "../screens/UpdateUser";
import WinnerList from "../screens/WinnerList";
import ExchangeCoinsScreen from "../screens/ExchangeCoinsScreen";
import PurchaseCoinScreen from "../screens/PurchaseCoinScreen";
import PostFeedScreen from "../screens/PostFeedScreen";
import Wallet from "../screens/Wallet";
import Playandwin from "../screens/Playandwin";
const { Navigator, Screen } = createStackNavigator();

function AuthNavigator() {
  return (
    <Navigator
      screenOptions={{ headerShown: false }}
    >
      <Screen name="HomeScreen" component={HomeScreen} />
      <Screen name="UpdateUser" component={UpdateUser} />
      <Screen name="WinnerList" component={WinnerList} />
      <Screen name="ExchangeCoinsScreen" component={ExchangeCoinsScreen} />
      <Screen name="PurchaseCoinScreen" component={PurchaseCoinScreen} />
      <Screen name="PostFeedScreen" component={PostFeedScreen} />
      <Screen name="Wallet" component={Wallet} />
      <Screen name="Playandwin" component={Playandwin} />
    </Navigator>
  );
}
export const AuthNavigation = () => (
  <NavigationContainer>
    <AuthNavigator />
  </NavigationContainer>
);

import * as Facebook from "expo-facebook";
import * as firebase from "firebase";
import * as Google from 'expo-google-app-auth';

export const loginWithFacebook = async () => {
  try {
    await Facebook.initializeAsync({
      appId: "1995957447232062",
      appName: "monkeyCoin",
    });

    const { type, token } = await Facebook.logInWithReadPermissionsAsync({
      permissions: ["public_profile","email"],
    });

    if (type === "success") {
      // Build Firebase credential with the Facebook access token.
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      // Sign in with credential from the Facebook user.
      firebase
        .auth()
        .signInWithCredential(credential)
        .then((res) => {
          console.log(res.user);
        })
        .catch((error) => {
          // Handle Errors here.
          console.log("success error");
          console.log(error);
        });
    }
  } catch (error) {
    console.log("error in try");
    console.log(error);
  }
};

export const loginWithGoogle = async () => {
  try {
    await Facebook.initializeAsync({
      appId: "1995957447232062",
      appName: "monkeyCoin",
    });

    const { type, accessToken, user } = await Google.logInAsync({
      androidClientId: `AIzaSyAe7e39qahkdkhdHShKJNoUGeBqf68RNGM`,
      androidStandaloneAppClientId: `AIzaSyAe7e39qahkdkhdHShKJNoUGeBqf68RNGM`,
      scopes: ['profile', 'email'],
    });
    if (type === "success") {
      // Build Firebase credential with the Facebook access token.
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      // Sign in with credential from the Facebook user.
      firebase
        .auth()
        .signInWithCredential(credential)
        .then((res) => {
          console.log(res.user);
        })
        .catch((error) => {
          // Handle Errors here.
          console.log("success error");
          console.log(error);
        });
    }
  } catch (error) {
    console.log("error in try");
    console.log(error);
  }
};
